package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Task;
import com.example.demo.repo.TaskRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class TaskService {

    @Autowired
    private TaskRepo taskRepo;

    public List<Task> getAllTasks() {
        return taskRepo.findAll();
    }

    public Task getOneTask(String id) {
        Task task = taskRepo.findOne(id);
        return task;
    }

    public ResponseEntity<?> addOneTask(Task task) {
        task.setStatus("PENDING");
        taskRepo.save(task);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<?> editTask(Task task) {
        taskRepo.save(task);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<?> updateStatus(Map<String, String> map) {
        Task task = getOneTask(map.get("id"));
        task.setStatus("DONE");
        taskRepo.save(task);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<?> deleteTask(Map<String, String> map) {
        Task task = getOneTask(map.get("id"));
        taskRepo.delete(task);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}