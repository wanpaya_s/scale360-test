package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Task;
import com.example.demo.service.TaskService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {
    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/api/task", method = RequestMethod.GET)
    public List<Task> getAllTask() {
        return taskService.getAllTasks();
    }

    @RequestMapping(value = "/api/task/{id}", method = RequestMethod.GET)
    public Task getSingleTask(@PathVariable String id) {
        return taskService.getOneTask(id);
    }

    @RequestMapping(value = "/api/task", method = RequestMethod.POST)
    public ResponseEntity<?> addOneTask(@RequestBody Task task) {
        return taskService.addOneTask(task);
    }

    @RequestMapping(value = "/api/task", method = RequestMethod.PATCH)
    public ResponseEntity<?> editTask(@RequestBody Task task) {
        return taskService.editTask(task);
    }

    @RequestMapping(value = "/api/task/status", method = RequestMethod.POST)
    public ResponseEntity<?> updateStatus(@RequestBody Map<String, String> map) {
        return taskService.updateStatus(map);
    }

    @RequestMapping(value = "/api/task", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTask(@RequestBody Map<String, String> map) {
        return taskService.deleteTask(map);
    }
}